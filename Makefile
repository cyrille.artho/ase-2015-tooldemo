#!/usr/bin/make -f
#
# $Id: Makefile,v 1.4 2007/04/04 04:45:45 cartho Exp $
#

.PHONY: all ps pdf clean pics runclean

# --- programs
TEX=latex
PDFTEX=pdflatex
DVIPS=dvips

# --- files

FILENAME=tooldemo
.SECONDARY: $(FILENAME).dvi

# ---------------------------------------------------------------------

all:	ps pdf

ps:	$(FILENAME).dvi $(FILENAME).ps pics

pdf:	$(FILENAME).pdf pics

# --- generic rules

%.eps: %.fig
	$(FIG2DEV) -L eps <$*.fig >$@

%.dvi: %.tex biblio.bib pics
	-$(TEX) $*.tex
	bibtex $*
	$(TEX) $*.tex
	$(TEX) $*.tex

%.ps:	$(FILENAME).dvi pics
	$(DVIPS) -f $*.dvi >$@	

%.pdf:	$(FILENAME).tex biblio.bib pics
	-$(PDFTEX) $*.tex
	bibtex $* 
	$(PDFTEX) $*.tex
	$(PDFTEX) $*.tex

# --- specific rules

pics:
	make -C dot
	make -C listings
	make -C pics

tar: ps pdf $(FILENAME).dvi
	F1=`cat ${FILENAME}.tex ipasir.tex | grep includegraphics | sed -e 's/.*{//' -e 's/}/.pdf/' -e s,^,${FILENAME}/,`; \
	F1=`cat ${FILENAME}.tex ipasir.tex | grep includegraphics | sed -e 's/.*{//' -e 's/}/.eps/' -e s,^,${FILENAME}/,`; \
	cd ..; \
	rm -f ${FILENAME}; ln -s ase-2015-tooldemo ${FILENAME}; \
	tar \
	-cvzf ${FILENAME}-artho.tar.gz \
	${FILENAME}/*.{tex,dvi,bib} ${FILENAME}/*.{cls,bst} \
	$${F1} $${F2}; \
	cd ${FILENAME}

runclean:
	rm -f $(FILENAME).log $(FILENAME).aux $(FILENAME).toc *.blg *.bbl *.out

# add figclean below once there are any figures
clean:	runclean
	rm -f $(FILENAME).dvi $(FILENAME).ps  $(FILENAME).pdf
	make -C dot clean
	make -C listings clean
	make -C pics clean

distclean: clean
