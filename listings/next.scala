def valid = (version == dataModel.version)

def next { // simplified for brevity
  require (valid)
  require (pos < dataModel.n-1)
  val res = SUTit.next
  pos += 1
  assert (dataModel.data(pos) == res)
} 
