/*------------------------------------------------------------------------*/
/* User defined.
 */
double font_size = 10;
double baseline_skip = 12;
double character_width = 6;
double border_width = 2;
double emptylineskip = 0.4;

/*------------------------------------------------------------------------*/

#include <assert.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

int x = 0, y = 0, max_y = 0;
FILE * input, * output;
char lines[100][100];

float width, height;

/*------------------------------------------------------------------------*/

int match (const char * str)
{
  char buffer[100];
  const char * p;
  char * q;

  p = str;
  q = buffer;
  while((isalnum(*p)) || (*p == '/') || (*p == '#') || (*p == '@') ||
	(*p == '\"'))
    *q++ = *p++;

  *q = 0;

  if(!strcmp(buffer, "package")) return strlen(buffer);
  if(!strcmp(buffer, "class")) return strlen(buffer);
  if(!strcmp(buffer, "double")) return strlen(buffer);
  if(!strcmp(buffer, "public")) return strlen(buffer);
  if(!strcmp(buffer, "private")) return strlen(buffer);
  if(!strcmp(buffer, "native")) return strlen(buffer);
  if(!strcmp(buffer, "synchronized")) return strlen(buffer);
  if(!strcmp(buffer, "void")) return strlen(buffer);
  if(!strcmp(buffer, "return")) return strlen(buffer);
  if(!strcmp(buffer, "int")) return strlen(buffer);
  if(!strcmp(buffer, "float")) return strlen(buffer);
  if(!strcmp(buffer, "boolean")) return strlen(buffer);
  if(!strcmp(buffer, "long")) return strlen(buffer);
  if(!strcmp(buffer, "char")) return strlen(buffer);
  if(!strcmp(buffer, "short")) return strlen(buffer);
  if(!strcmp(buffer, "byte")) return strlen(buffer);
  if(!strcmp(buffer, "this")) return strlen(buffer);
  if(!strcmp(buffer, "new")) return strlen(buffer);
  if(!strcmp(buffer, "try")) return strlen(buffer);
  if(!strcmp(buffer, "catch")) return strlen(buffer);
  if(!strcmp(buffer, "finally")) return strlen(buffer);
  if(!strcmp(buffer, "if")) return strlen(buffer);
  if(!strcmp(buffer, "else")) return strlen(buffer);
  if(!strcmp(buffer, "while")) return strlen(buffer);
  if(!strcmp(buffer, "break")) return strlen(buffer);
  if(!strcmp(buffer, "throw")) return strlen(buffer);
  if(!strcmp(buffer, "static")) return strlen(buffer);
  if(!strcmp(buffer, "extends")) return strlen(buffer);
  if(!strcmp(buffer, "implements")) return strlen(buffer);
  if(!strcmp(buffer, "import")) return strlen(buffer);
  if(!strcmp(buffer, "val")) return strlen(buffer);
  if(!strcmp(buffer, "var")) return strlen(buffer);
  if(!strcmp(buffer, "object")) return strlen(buffer);
  if(!strcmp(buffer, "implicit")) return strlen(buffer);
  if(!strcmp(buffer, "abstract")) return strlen(buffer);
  if(!strcmp(buffer, "def")) return strlen(buffer);
  if(!strcmp(buffer, "override")) return strlen(buffer);
  if(!strcmp(buffer, "case")) return strlen(buffer);
  if(!strcmp(buffer, "type")) return strlen(buffer);
  if(!strcmp(buffer, "trait")) return strlen(buffer);
  if(!strcmp(buffer, "\"main\"")) return -strlen(buffer);

  return 0;
}

/*------------------------------------------------------------------------*/

int main(int argc, char ** argv)
{
  int i, j, ch, col, len;
  float line;
  int prevchar;
  int emptylines;
  int escape;

  input = (argc > 1) ? fopen (argv[1], "r") : stdin;
  if(!input) exit (1);

  output = stdout;

  emptylines = -1; // do not count last line
  escape = 0;
  while(!feof(input))
    {
      y = 0;
      escape = 0;

      while((ch = fgetc(input)) != '\n' && ch != EOF) {
        lines[x][y++] = ch;
	if (ch == '\\')
	  escape++;
      }

      assert(y < sizeof(lines[0]));

      if (y - escape > max_y) max_y = y;

      if (y || ch == '\n')
	lines[x++][y] = 0;

      if (y == 0)
	emptylines++;

      assert(x < sizeof(lines)/sizeof(lines[0]));
    }

#if 0
  for (i = 0; i < x; i++)
    {
      j = 0;

      while((ch = lines[i][j++]))
	fputc (ch, stdout);

      fputc ('\n', stdout);
    }
#endif

  width = max_y * character_width + 2 * border_width;
  height = font_size + (x - 1) * baseline_skip + 2 * border_width;
  height = height - (emptylines * (1.0 - emptylineskip)) * baseline_skip;

  fprintf (output, 
    "%%!PS-Adobe-2.0 EPSF-2.0\n"
    "%%%%BoundingBox: 0 0 %.0f %.0f\n"
    "%%%%EndComments\n", width, height);

  fprintf(output,
    "/F {findfont %.0f scalefont setfont} bind def\n"
    "/B {/Courier-Bold F moveto show} bind def\n"
    "/I {/Courier-Italic F moveto show} bind def\n"
    "/R {1 0 0 setrgbcolor /Courier-Bold F moveto show} bind def\n"
    "/C {/Courier F moveto show} bind def\n",
    font_size);

  fprintf(output, "%%%%Page: 1 1\n");

  line = 0.0;
  for (i = 0; i < x; i++)
    {
      j = 0;
      col = 0;
      len = 0;
      prevchar = 0;
      escape = 0;

      while ((ch = lines[i][j]))
	{
	  if (!escape && ch == '\\') {
	    escape = 1;
	    j++;
	    continue;
	  }
	  if (escape) {
	    escape = 0;
	  }
	  fputc ('(', stdout);
	  if (ch == '(' || ch == ')' || ch == '\\')
	    fputc ('\\', stdout);
	  fprintf (output, "%c)", ch);
	       
	  fprintf (output, " %.0f %.0f", 
	     col * character_width + border_width,
	     height - line * baseline_skip - font_size - border_width);

	  if (len > 0)
	    len--;
	  else if (len < 0)
	    len++;
	  else
	    {
	      len = match (lines[i] + j);
	      if (isalpha(prevchar))
		len = 0;
	    }

	  fprintf (output, " %c\n", (len > 0) ? 'B' : (len == 0) ? 'C' : 'I');

	  j++;
	  col++;
	  prevchar = ch;
	}
      if (j) {
	line += 1.0;
      } else {
	line += emptylineskip;
      }
// used to reset color
/*	if (len < 0)
	  fprintf (output,  "0 0 0 setrgbcolor\n");*/
    }

  if(argc > 1) fclose(input);

  exit(0);
  return 0;
}
