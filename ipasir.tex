\newcommand{\model}[1]{\textsf{#1}}

\subsection{SAT Solvers in Verification}

SAT solvers are tools for deciding the satisfiability 
problem of propositional logic. 
Formulas of propositional logic consist of atomic variables $x, y, \ldots$
defined over the Boolean domain, 
logical connectives like negation, conjunction and disjunction
with standard semantics, as 
well as parentheses necessary to structure a formula.
For example, $(x \vee \neg y) \wedge (\neg x \vee y)$ is true 
if both variables have the same value. A SAT solver tries to 
find an assignment for the variables of a formula such that the 
formula is true under this assignment.
By being the prototypical 
problem for the complexity class NP, SAT offers a powerful 
framework for encoding and solving many problems stemming 
from verification, artificial intelligence,
etc.~\cite{DBLP:series/faia/2009-185}.
Often SAT solvers 
are not used only once in a verification problem but in an incremental 
manner. Here a satisfiable formula is enriched with additional constraints 
until it becomes either unsat or the considered problem is found 
to be satisfiable. Usually formulas are represented in 
conjunctive normal form (CNF), i.\,e., as conjunction of clauses. 
A clause is a disjunction of literals and a literal is either 
a variable or its negation. Constraints are given in form of 
additional clauses. 
 
For developing a competitive SAT solver that is able 
to handle real-world formula instances, 
pruning techniques are essential.
These techniques have to 
be carefully integrated into the incremental solving process
in order to preserve the correctness of the solver. Therefore, 
modern SAT solvers are very complex pieces of software, and as it 
has been shown they are not resistant to errors. Especially when SAT solvers 
are part of the verification process, however, it is not acceptable that 
a SAT solver is faulty, because otherwise the whole effort 
put into the verification process is useless. Because of their
complex structure,
competitive SAT solvers are not amenable to full verification. Therefore, other 
techniques have to be applied to ensure correctness and trust in 
SAT solvers. 

\subsection{Model-based Testing of SAT Solvers}
In previous work, we have presented model-based testing for SAT
solvers~\cite{ArthoBiereSeidl-TAP13}.
To this end, we considered a data model describing expressive random formulas 
 as input as well as a model describing the usage 
of the API. We presented specific solutions for the SAT solver
Lingeling~\cite{biere2014yet},
which has always ranked highly in the SAT solver competition because of its 
sophisticated pruning techniques. In various experiments 
we showed the power of model-based testing for the SAT solver
Lingeling~\cite{ArthoBiereSeidl-TAP13}.
Those models are very specific to Lingeling, which provides 
an extensive API and many options, and hence cannot be used for other 
SAT solvers directly. At that time no standards for incremental SAT 
solver APIs existed. 

This year the SAT competition offers a special track for incremental 
solving, acknowledging its practical relevance. In this track, submissions 
include incremental solvers as well as applications which use 
incremental SAT solvers. To this end, a standardized interface 
called IPASIR 
has been specified which the participating SAT solvers have to implement 
and which can be used by the applications~\cite{sat-race-2015}.
We contribute to the 
incremental track of the SAT solver competition by providing a 
model-based tester for incremental SAT solvers. 

\begin{figure}[t!]
\begin{center}
\includegraphics[scale=0.55,angle=-90]{dot/IPASIRModel}
\caption{IPASIR model for incremental SAT solvers; coverage after $5$ tests.}
\label{fig:sat}
\end{center}
\end{figure}

We implemented this 
model-based tester with Modbat by specifying the model shown in 
Figure~\ref{fig:sat}. The transitions between the states trigger
the call of various solver API functions specified by the IPASIR 
interface. First, the solver is initialized,
and then the input formula is generated (left hand side). The input 
formula consists of unit clauses (clauses of size one), binary 
clauses, ternary clauses and clauses of arbitrary size. It is important 
to ensure that the formula contains clauses of size smaller than four 
because these clauses are often handled in a different manner. 
The literals and their polarity (negated/not negated) are randomly 
selected and given to the solver under test. After enough clauses 
have been generated, the solving function is called. As formulas are
generated at random, the outcome of the SAT solver cannot be predicted
by the model. The dashed transitions from state \model{solve} indicate
alternative outcomes (unsatisfiable formulas or a time-out) overriding
the default successor state \model{sat}, with \model{unsat} and \model{inc},
respectively, using \texttt{nextIf} statements~\cite{artho-hvc2013}
that specify pairs of predicates and successor states. If we use option
\texttt{-{}-dotify-coverage}, Modbat indicates that the first five tests
generate only unsatisfiable formulas (see Figure~\ref{fig:sat}).

The new model can be used for any solver 
implementing the IPASIR interface. Therefore, it can be easily 
integrated in the solver development process. 
For the competition we suggest to 
measure the time a solver takes to finish a given number of tests
generated from a fixed random seed. 

In the provided demo,
we test SAT solver PicoSAT~\cite{picosat}.
We demonstrate the tests on the original version $961$ and a modified 
version, where we introduced a small 
bug in a pruning technique called 
\emph{failed literal probing} (in the faulty version, a literal is not negated). 
The bug causes the program to crash sporadically. With Modbat
this bug can be found quickly. 
Note that Picosat can be easily exchanged against any incremental SAT
solver implementing the IPASIR interface.
