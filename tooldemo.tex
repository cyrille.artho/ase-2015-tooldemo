%% LyX 2.1.3 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[english,conference]{IEEEtran}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{array}
\usepackage{url}
\usepackage{multirow}
\usepackage{graphicx}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
%% Because html converters don't know tabularnewline
\providecommand{\tabularnewline}{\\}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{alltt}
\usepackage[multiple]{footmisc}
\usepackage{balance}

\makeatother

\usepackage{babel}
\begin{document}

\title{Model-based Testing of Stateful APIs with Modbat}


\author{\IEEEauthorblockN{Cyrille Artho\IEEEauthorrefmark{1}, Martina Seidl\IEEEauthorrefmark{2},
Quentin Gros\IEEEauthorrefmark{3}, Eun-Hye Choi\IEEEauthorrefmark{1},\\
Takashi Kitamura\IEEEauthorrefmark{1}, Akira Mori\IEEEauthorrefmark{1},
Rudolf Ramler\IEEEauthorrefmark{4}, and Yoriyuki Yamagata\IEEEauthorrefmark{4}}
\IEEEauthorblockA{\IEEEauthorrefmark{1}National Institute of Advanced Industrial Science and Technology (AIST), Amagasaki, Japan\\
\{c.artho,e.choi,t.kitamura,a-mori,yoriyuki.yamagata\}@aist.go.jp}\IEEEauthorblockA{\IEEEauthorrefmark{2}Johannes
Kepler University, Linz, Austria\\
martina.seidl@jku.at}\IEEEauthorblockA{\IEEEauthorrefmark{3}University
of Nantes, Nantes, France\\
quentin.gros@etu.univ-nantes.fr}\IEEEauthorblockA{\IEEEauthorrefmark{4}Software
Competence Center Hagenberg, Hagenberg, Austria\\
rudolf.ramler@scch.at}}
\maketitle
\begin{abstract}
Modbat makes testing easier by providing a user-friendly modeling
language to describe the behavior of systems; from such a model, test
cases are generated and executed. Modbat's domain-specific language
is based on Scala; its features include probabilistic and non-deterministic
transitions, component models with inheritance, and exceptions. We
demonstrate the versatility of Modbat by finding a confirmed defect
in the currently latest version of Java, and by testing SAT solvers.\end{abstract}

\begin{IEEEkeywords}
model-based testing; software test tools; domain-specific language;
extended finite-state machines; component-based systems; exception
testing
\end{IEEEkeywords}


\section{Introduction}

Model-based testing derives test cases from an abstract model of the
system under test and/or its environment~\cite{utting06}. The tool
Modbat presented in this paper supports model-based testing by providing
a domain-specific language (DSL) to define high-level models in a
user-friendly way. Extended finite-state machines (EFSMs) combine
a high-level model with extra variables and transition functions~\cite{utting06}.
Test cases are generated as sequences of method calls to the application
programming interface (API) of the system under test (SUT). Results
can be checked using assertions, or stored in model variables, to
be used in subsequent calls.

Our DSL significantly reduces the notational overhead compared to
other tools\ \cite{artho-hvc2013}. Exceptions are managed on the
model level, which increases the clarity of the model. Non-determinism
in a system, for example from communication delays in a network, is
also directly handled in the model. This makes Modbat particularly
suitable to test state-based systems with potential non-determinism,
such as networked applications~\cite{artho-hvc2013,artho-ase2013}.
A model can easily be visualized by generating a graph representation
using graphviz~\cite{Gansner00anopen} for a quick overview of all
states and transitions. Component-based systems can be modeled using
multiple state machines, which are executed in an interleaving way
to simulate (stepwise) parallel actions on components. Finally, Modbat
provides debugging support on multiple levels by writing error traces
into a log and also offering test replay and off-line code generation.

This paper is organized as follows: Section~\ref{sec:Modbat's-Architecture}
gives an overview of Modbat. Sections~\ref{sec:Scenario-1:-Java}
and~\ref{sec:Scenario-2:-SAT} outline our two demonstrations, on
the Java collection classes and the API of a SAT solver (written in
C). Section~\ref{sec:Related-Work} compares Modbat with related work. Section~\ref{sec:Discussion-and-Conclusion}
discusses Modbat's potential impact on software testing and concludes.


\section{Modbat's Architecture and Syntax\label{sec:Modbat's-Architecture}}

Modbat models are written in an internal (embedded) DSL~\cite{wampler2009programming}
using Scala~\cite{Odersky2008} as host language. We have chosen Scala
because of its flexible syntax~\cite{arthoicfem2015} and because
it runs on the Java VM, which makes it easy to test programs written
in Java, Scala, C, and other languages\ \cite{artho-hvc2013}. The
DSL mixes deep embedding~\cite{wampler2009programming}, which defines
its own data structure for the abstract syntax tree of the model's state machine, 
with shallow embedding for model variables and test
actions. This makes the language very concise for the state machine
and at the same time allows accessing the full range of Scala and Java libraries
from the model.

\begin{figure}
\begin{centering}
\includegraphics[scale=0.4]{pics/modbat-arch}
\par\end{centering}

\protect\caption{Architecture and workflow of Modbat.\label{fig:Overview-of-Modbat}}
\end{figure}


On an architectural level, the workflow consists of two steps (see
Figure~\ref{fig:Overview-of-Modbat}): First, the model is compiled
against the model library provided by Modbat. The model library defines
the DSL in terms of type conversions and custom operators for the
deeply embedded part of the DSL, combined with code that can access
Modbat's API in test actions, which are written as Scala or Java code.
Second, tests are generated. At run-time Modbat loads the model and
explores it using a random search, executing the SUT in tandem. The
sequence of transitions executed between the initial and final model
states constitutes a test run. After each test run, the model and
the SUT are reset to their initial state. Upon failure, Modbat emits
the error trace showing the execution history.


\subsection{Example: Java's \texttt{ArrayList}}

Figure~\ref{fig:Simple-list-model} shows the syntax of Modbat's
DSL on a partial model of Java's \texttt{ArrayList}. Each Modbat model
extends \texttt{modbat.dsl.Model}, which defines the DSL. Variables
include a reference to the SUT and model variables to track the model's
view of the SUT to verify its results. In this case the model only
keeps track of the expected size of the list. The model code can also
define a number of functions (in the middle) that can be referenced
from the declared transitions (at the bottom). This example uses only
one model state, \emph{main}, because most actions are available in
any state.\footnote{It is also possible to use multiple states in the model, such as \emph{empty}
and \emph{non-empty,} and to define transitions with pre- and postconditions.} To use random data, the model calls Modbat's \texttt{choose} functions,
which return a random number and a random element in a list of functions,
respectively.

\begin{figure}
\includegraphics[scale=0.65]{listings/SimpleListModel}

\protect\caption{Simple list model.\label{fig:Simple-list-model}}


\end{figure}


The example shows that test code can be kept in a separate function
or be directly written as an anonymous function. We can also modify
the \emph{weight} of a transition function; by default its value is
$1$. In this example, we emphasize adding elements by giving more weight to \texttt{add} so calls to \texttt{clear}
do not repeatedly empty the list before several elements can be added.
Finally, we can easily declare that an exception \emph{must} occur
in a given transition. In this case, calls to \texttt{remove} with
index $-1$ or $n$ access an entry outside the valid range, so the SUT is expected 
to throw a corresponding exception.


\subsection{Key Features of Modbat}

Modbat's light-weight DSL has been inspired by a preprocessor to ModelJUnit~\cite{artho-eurocast2009}.
Compared to similar tools, it is more concise and expressive for models
that are based on transition systems~\cite{artho-hvc2013}, especially
for non-deterministic actions like non-blocking network input/output,
where the result of an operation depends not only on inputs but also
on the physical state of the network~\cite{artho-ase2013}. As models
are Scala classes, they inherit all variables, functions, and transitions
in a natural way, which makes Modbat ideal for testing libraries implementing
several related data structures or protocols. Furthermore, the current
version of Modbat introduces \emph{observer state machines,} similar
to abstract state machines in Spec Explorer~\cite{Veanes07model-basedtesting}. 

In this demonstration, we take advantage of being able to use multiple
models in parallel. Unlike in other tools~\cite{Veanes07model-basedtesting},
the number of parallel models does not have to be fixed a priori.
Instead, models are instantiated dynamically with function \texttt{launch},
which initializes a new (possibly parametrized) model. Newly launched
models become active at the end of the current transition. 


\section{Scenario 1: Java Iterators\label{sec:Scenario-1:-Java}}

The Java library contains \emph{collections,} data structures for
data types such as lists, sets, and maps~\cite{J2SE}. Iterators
provide a way to access elements of a collection one by one.


\subsection{Semantics of the Iterator API}

Iterators can be instantiated on an underlying collection through the 
methods \texttt{iterator} and \texttt{listIterator}. The former provides
a simple forward iterator (see Table~\ref{tab:Iterator-methods}),
while the latter provides a bidirectional iterator (see Table~\ref{tab:list-iterator-methods})~\cite{J2SE}.

\begin{table}
\protect\caption{Iterator methods.\label{tab:Iterator-methods}}


\begin{centering}
\begin{tabular}{|l|l|}
\hline 
Method & Description\tabularnewline
\hline 
\hline 
\texttt{hasNext} & true if forward iterator has more elements\tabularnewline
\hline 
\texttt{next} & returns the next element and advances the cursor\tabularnewline
\hline 
\texttt{remove} & removes the element that was returned (optional method)\tabularnewline
\hline 
\end{tabular}
\par\end{centering}

\end{table}


\begin{table}
\protect\caption{Additional list iterator methods.\label{tab:list-iterator-methods}}


\centering{}%
\begin{tabular}{|l|l|}
\hline 
Method & Description\tabularnewline
\hline 
\hline 
\texttt{add }\texttt{\emph{e}} & inserts the specified element (optional method)\tabularnewline
\hline 
\texttt{hasPrevious} & true if reverse iterator has more elements\tabularnewline
\hline 
\texttt{nextIndex} & returns the index of the next element\tabularnewline
\hline 
\texttt{previous} & return the previous element, moves the cursor backwards\tabularnewline
\hline 
\texttt{previousIndex} & returns the index of the previous element\tabularnewline
\hline 
\texttt{set }\texttt{\emph{e}} & replaces the element that was returned (optional method)\tabularnewline
\hline 
\end{tabular}
\end{table}


Java iterators do not allow a concurrent modification of the underlying
collection while iterating on it. Any modification of the underlying
collection \emph{invalidates} all previously created iterators on
it. Invalid iterators produce an undefined result for calls to \texttt{hasNext}
and similar methods,\footnote{This was confirmed by Oracle as a response to a bug report filed by
us: \url{http://bugs.java.com/bugdatabase/view_bug.do?bug_id=JDK-8129758}.} and throw a \texttt{ConcurrentModificationException} if attempts
are made to access or modify data through them. Through experiments
we confirmed that this exception is usually only thrown upon a \emph{successful}
modification of the underlying collection.

While directly modifying the collection invalidates all of its iterators,
it is possible to modify data, if the iterator itself provides a set
of optional methods (\texttt{add}, \texttt{set}, and \texttt{remove}).
Such modifications are intricately linked with iteration: \texttt{remove}
and \texttt{set} both require a preceding call to either \texttt{next}
or \texttt{previous}. Furthermore, calls to \texttt{remove} or \texttt{add}
require another iterator step before \texttt{remove} or \texttt{set}
can be called again. We discuss this property in more depth below.


\subsection{Model of the Iterator API}

Our list model closely mirrors Java's collections but uses simpler
data structures to ensure correctness. We use random data as items
to be added and also add a function to validate internal model invariants.
Modbat's support for inheritance is very useful here because \texttt{ArrayList}
implements a strict subset of all operations in \texttt{LinkedList}.\footnote{Some operations are not provided by \texttt{ArrayList} because they
cannot be implemented efficiently on arrays.} Our generic list model uses the following data structures: \texttt{testData}
(the SUT); \texttt{data}, a fixed-size array that models the list
contents; \texttt{n}, which counts the number of elements; and \texttt{version},
which counts the number of changes to the collection. We use preconditions
to distinguish between cases where a method can be used successfully,
and cases where we expect an exception to be thrown. Modbat's direct
support for exceptions in transitions allows us to express these features
succinctly~\cite{artho-hvc2013}.

\begin{figure}


\begin{centering}
\includegraphics[scale=0.4]{pics/it-testing}
\par\end{centering}

\protect\caption{Orchestrating collection and iterator models.\label{fig:Orchestrating-models}}


\end{figure}


\begin{figure}
\begin{centering}
\includegraphics[angle=-90,scale=0.6]{dot/listiterator}
\par\end{centering}

\protect\caption{Model of the bidirectional iterator showing the applicability of \texttt{set}
and \texttt{remove} before and after calls to other methods.\label{fig:iterator-model}}


\end{figure}


Iterators and list iterator models are instantiated by transitions
in the list model that launch a new child model instance, and link
it to the resulting iterator that is obtained from the SUT. Each model
may affect the collection and/or an iterator (see Figure~\ref{fig:Orchestrating-models}).
The iterator models remember the \texttt{version} of the list so the
occurrence or absence of a \texttt{ConcurrentModificationException}
can be modeled based on whether the version counts of the collection
and of the iterator match. Furthermore, we use a state ``modifiable''
to model whether calls to \texttt{set} and \texttt{remove} are permitted;
these calls require a previous call to \texttt{next} or \texttt{previous},
without any other modification in between (see Figure~\ref{fig:iterator-model}). 

Other requirements are captured using preconditions and postconditions.
We choose to model valid and invalid usage contexts using mutually
exclusive preconditions, and verify the correct result using exception
declarations and postconditions (see Figure~\ref{fig:Transition-modeling-next};
details can be found online~\cite{modbat}).

\begin{figure}[t]
\includegraphics[scale=0.65]{listings/next}

\protect\caption{Transition modeling valid uses of \texttt{next}.\label{fig:Transition-modeling-next}}


\end{figure}



\subsection{Defect Found in Java 1.8}

When running the test model against Java's list implementation, some
tests fail on \texttt{ArrayList}: After a failed \texttt{remove(-1)},
the Java library marks the list as modified; subsequent calls to \texttt{next}
throw a \texttt{ConcurrentModificationException} (see Figure~\ref{fig:Error-trace-for-arraylist}).
Other types of failed modifications (such as \texttt{remove} with
$n\geq0$) do \emph{not} mark the list as modified. All other data
structures behave consistently in all cases. Modbat finds the problem
quickly; Oracle has confirmed the issue as a defect.\footnote{\url{https://bugs.openjdk.java.net/browse/JDK-8114832}}

\begin{figure}
{\footnotesize{}\begin{alltt}
ArrayList<Integer> list = new ArrayList<Integer>;
Iterator<Integer> \textbf{it = list.iterator();}
try \{
    \textbf{list.remove(-1);}           \textit{// attempt removal}
\} catch (IndexOutOfBoundsException) \{\}   \textit{// fails}
\textbf{it.next();}    \textit{// expected: NoSuchElementException}
    \textit{// but throws ConcurrentModificationException}
\end{alltt}}{\footnotesize \par}

\protect\caption{Error trace for Java's \texttt{ArrayList}. We obtain an iterator on
an empty list, and then try to remove a non-existent element at index
$-1$. This operation does not modify the list, but the next operation
throws a \texttt{ConcurrentModificationException}, which is wrong.
The trace shown here is minimal, but the issue is more serious for
non-empty lists, because the spurious exception prevents further access
to data.\label{fig:Error-trace-for-arraylist}}


\end{figure}



\section{Scenario 2: SAT Solver\label{sec:Scenario-2:-SAT}}

\input{ipasir.tex}

\input{related-work.tex}


\section{Discussion and Conclusion\label{sec:Discussion-and-Conclusion}}

ModelJUnit~\cite{utting06} helped to bring model-based testing to
early adopters of test case generation technology. Modbat has been
inspired by ModelJUnit~\cite{artho-eurocast2009} and has been created
to simplify some of the modeling tasks, such as specifying transitions
with their preconditions and checking the occurrence of exceptions
in actions~\cite{artho-hvc2013}. It has been successfully used to
model complex systems like SAT solvers~\cite{artho-hvc2013}, where
it replaced a custom test generator written in C~\cite{ArthoBiereSeidl-TAP13},
and the Java network library, including non-deterministic actions
like non-blocking input/output~\cite{artho-ase2013}. This demonstration
shows Modbat on Java's iterators, where we found a new previously
unknown defect, and on a new API for incremental SAT solving. Modbat's
flexible DSL makes it possible to express both models succinctly and
clearly. This facilitates focusing on the semantics
of a system, which reduces the model development time and the risk of introducing 
defects in the model. We think that Modbat contributes to making
model-based testing more applicable to complex software, and we hope
that with an open source release of Modbat, model-based testing will
become more widespread.

Even with an elegant and expressive modeling platform, writing a model
that includes an output oracle requires an in-depth understanding
of the system. Good model design is not always straightforward. In
our experience, cognitive bias has sometimes prevented us from modeling
the full state space in an initial version of the model~\cite{artho-ftscs2013}.
We plan to add graphical tool support, and more ways to visualize
model traces and their code coverage in the future, to mitigate this
problem. 

Modbat is available for download~\cite{modbat}. The material for this
demonstration can be found online~\cite{modbat-demo}.


\section*{Acknowledgements}

Part of the work was supported by the Japanese Society for the Promotion
of Science (\emph{kaken-hi} grants 23240003 and 26280019), by the
Austrian Science Fund (FWF) through the national research network
RiSE (S11408-N23), by the Austrian Ministry for Transport, Innovation
and Technology, the Federal Ministry of Science, Research and Economy,
and the Province of Upper Austria in the frame of the COMET center
SCCH.

\cleardoublepage{}

\balance\bibliographystyle{IEEEtran}
\bibliography{biblio}

\end{document}
